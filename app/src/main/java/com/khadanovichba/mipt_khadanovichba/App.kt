package com.khadanovichba.mipt_khadanovichba
import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class App : Application()