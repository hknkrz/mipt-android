package com.khadanovichba.mipt_khadanovichba.Interface.Screens

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Card
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.khadanovichba.mipt_khadanovichba.Interface.ViewModels.RestaurantsViewModel
import coil.compose.AsyncImage
import com.khadanovichba.mipt_khadanovichba.Data.Restaurant
import java.net.URLEncoder
import androidx.hilt.navigation.compose.hiltViewModel


@Composable
fun RestaurantScreen(navController: NavController) {
    val restaurantViewModel = hiltViewModel<RestaurantsViewModel>()
    val nullable_state by restaurantViewModel.state.observeAsState()
    val state = nullable_state ?: return

    LazyColumn(verticalArrangement = Arrangement.spacedBy(20.dp)) {
        item { Text(text = "Popular Restaurants", fontSize = 40.sp, textAlign = TextAlign.Center)}
        items(state.popularRestaurants) { restaurant ->
            ShowRestaurant(restaurant = restaurant, navController)
        }
        item {Text(text = "Nearest Restaurants", fontSize = 40.sp, textAlign = TextAlign.Center)}
        items(state.nearestRestaurants) {
                restaurant ->
            ShowRestaurant(restaurant = restaurant, navController)
        }
    }
}

@Composable
fun ShowRestaurant(restaurant: Restaurant, navController: NavController) {
    Card(
        modifier = Modifier
            .height(220.dp)
            .fillMaxWidth()
            .padding(15.dp)
    ) {
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            Text(text = restaurant.name, fontWeight = FontWeight.Bold, fontSize = 20.sp)
            AsyncImage(
                model = restaurant.image,
                contentDescription = restaurant.name,
                contentScale = ContentScale.FillWidth,
                modifier = Modifier
                    .size(120.dp)
                    .padding(top = 10.dp)
                    .align(Alignment.CenterHorizontally)
                    .clickable {
                        navController.navigate("Restaurants/${restaurant.name}/${restaurant.time}/${URLEncoder.encode(restaurant.image, "UTF-8")}")
                    }
            )
            Text(text = restaurant.time)
        }
    }
}