package com.khadanovichba.mipt_khadanovichba.Interface.Screens

import LoginViewModel
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.*
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavController



@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun StartScreen(navController: NavController) {
    val viewModel: LoginViewModel = viewModel()
    val state by viewModel.state.collectAsState()

    Column(modifier = Modifier.padding(10.dp),
        horizontalAlignment = Alignment.CenterHorizontally) {
        Image(
            painter = painterResource(id = com.khadanovichba.mipt_khadanovichba.R.drawable.food_ninja),
            contentDescription = null,
            contentScale = ContentScale.Fit,
            modifier = Modifier
                .width(250.dp)
                .padding(50.dp)
                .height(200.dp)
        )
        Text(
            text = "Login To Your Account",
            textAlign = TextAlign.Center,
            style = TextStyle(
                fontSize = 22.sp,
                fontWeight = FontWeight.Bold,
            ),
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 50.dp)
                .padding(bottom = 20.dp)


        )

        TextField(
            colors = TextFieldDefaults.textFieldColors(
                textColor = Color.Black,
                disabledTextColor = Color.Gray,
                containerColor = Color.White,
                focusedIndicatorColor = Color.Transparent,
                unfocusedIndicatorColor = Color.Transparent,
                disabledIndicatorColor = Color.Transparent
            ),
            value = state.login,
            onValueChange = { viewModel.onUserLoginEvent(UserLoginEvent.EmailInput(it)) },
            placeholder = { Text( text = "aboba@example.com") },
            singleLine = true,
            modifier = Modifier
                .padding(bottom = 20.dp)
                .border(1.dp, Color.Gray, RoundedCornerShape(20.dp))                .height(70.dp)
                .width(330.dp)
        )

        TextField(
            colors = TextFieldDefaults.textFieldColors(
                textColor = Color.Black,
                disabledTextColor = Color.Gray,
                containerColor = Color.White,
                focusedIndicatorColor = Color.Transparent,
                unfocusedIndicatorColor = Color.Transparent,
                disabledIndicatorColor = Color.Transparent
            ),
            value = state.password,
            onValueChange = { viewModel.onUserLoginEvent(UserLoginEvent.PasswordInput(it)) },
            placeholder = { Text( text ="your password") },
            singleLine = true,
            visualTransformation = PasswordVisualTransformation(),
            modifier = Modifier
                .padding(bottom = 15.dp)
                .border(1.dp, Color.Gray, RoundedCornerShape(20.dp))
                .height(70.dp)
                .width(330.dp)

        )
        Button(
            onClick = { navController.navigate("Restaurants") },
            colors = ButtonDefaults.buttonColors(
                containerColor = Color.Green
            ),
            content = {
                Text(
                    text = "Login",
                    style = TextStyle(
                        color = Color.White,
                        fontSize = 20.sp,
                        fontWeight = FontWeight.Bold
                    )
                )
            },
            modifier = Modifier
                .padding(top = 15.dp)
                .width(200.dp)
                .height(80.dp)
        )

    }

}