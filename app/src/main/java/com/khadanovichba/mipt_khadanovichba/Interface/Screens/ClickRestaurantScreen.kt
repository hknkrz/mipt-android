package com.khadanovichba.mipt_khadanovichba.Interface.Screens

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import coil.compose.AsyncImage
import coil.decode.SvgDecoder
import coil.request.ImageRequest
import java.net.URLDecoder
import java.nio.charset.StandardCharsets

@Composable
fun ClickRestaurant(
    name: String,
    time: String,
    imagePath: String
) {
    Box(modifier = Modifier.fillMaxSize()) {
        val decodedUrl = URLDecoder.decode(imagePath, StandardCharsets.UTF_8.toString())
        val modifier = Modifier.align(Alignment.Center)
        Box(
            modifier = modifier
                .height(250.dp)
                .width(250.dp)
                .background(Color.White)
        ) {
            Column(modifier = modifier.fillMaxWidth()
            ) {
                Text(
                    modifier = Modifier
                        .padding(top = 10.dp)
                        .align(Alignment.CenterHorizontally),
                    text = name
                )
                AsyncImage(model = ImageRequest.Builder(LocalContext.current)
                    .data(decodedUrl)
                    .decoderFactory(SvgDecoder.Factory())
                    .build(),
                    contentDescription = null,
                    contentScale = ContentScale.FillBounds,
                    modifier = Modifier
                        .padding(top = 30.dp)
                        .size(120.dp)
                        .align(Alignment.CenterHorizontally),
                )

                Text(
                    modifier = Modifier
                        .padding(top = 10.dp)
                        .align(Alignment.CenterHorizontally),
                    text = time
                )
            }
        }
    }
}