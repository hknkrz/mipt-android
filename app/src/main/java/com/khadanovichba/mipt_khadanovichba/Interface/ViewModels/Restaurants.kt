package com.khadanovichba.mipt_khadanovichba.Interface.ViewModels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.khadanovichba.mipt_khadanovichba.Data.CatalogRepository
import com.khadanovichba.mipt_khadanovichba.Data.Restaurant
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.flow.collectLatest

data class RestaurantsState(
        val nearestRestaurants: List<Restaurant> = emptyList(),
        val popularRestaurants: List<Restaurant> = emptyList()
)

@HiltViewModel
class RestaurantsViewModel @Inject constructor(
    private val repository: CatalogRepository) : ViewModel() {
    private val _state: MutableLiveData<RestaurantsState> = MutableLiveData(RestaurantsState())
    val state: LiveData<RestaurantsState> = _state

    init {
        viewModelScope.launch(Dispatchers.Default) {
            repository.getCatalog()
                .collectLatest { response ->
                    _state.postValue(
                        _state.value?.copy(
                            nearestRestaurants = response.nearest,
                            popularRestaurants = response.popular
                        )
                    )
                }
        }
    }

}