import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update

data class UserLoginState(
    val login: String = "",
    val password: String = "",
)

sealed class UserLoginEvent {
    data class EmailInput(val value: String) : UserLoginEvent()
    data class PasswordInput(val value: String) : UserLoginEvent()
}

class LoginViewModel: ViewModel() {
    private val _state = MutableStateFlow(UserLoginState())
    val state = _state.asStateFlow()

    fun onUserLoginEvent(event: UserLoginEvent) {
        when (event) {
            is UserLoginEvent.EmailInput -> onUserLoginInput(event.value)
            is UserLoginEvent.PasswordInput -> onUserPasswordInput(event.value)
            else -> {}
        }
    }

    private fun onUserLoginInput(value: String) {
        _state.update {
            it.copy(
                login = value
            )
        }
    }

    private fun onUserPasswordInput(value: String) {
        _state.update {
            it.copy(
                password = value
            )
        }
    }
}