package com.khadanovichba.mipt_khadanovichba.Data

import kotlinx.serialization.Serializable


@Serializable
data class Catalog(
    val nearest: List<Restaurant>,
    val popular: List<Restaurant>,
    val commercial: Commercial?
)

@Serializable
data class Restaurant(
    val id: Int,
    val name: String,
    val time: String,
    val image: String
)

@Serializable
data class Commercial(
    val picture: String,
    val url: String
)

fun Restaurant.toNearestEntity():
        NearestEntity = NearestEntity(id=id, name=name, logo=image, time=time)


fun Restaurant.toPopularEntity():
        PopularEntity = PopularEntity(id=id, name=name, logo=image, time=time)
