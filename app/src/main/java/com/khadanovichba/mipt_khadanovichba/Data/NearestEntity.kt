package com.khadanovichba.mipt_khadanovichba.Data

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "nearest")
data class NearestEntity(
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "logo") val logo: String,
    @ColumnInfo(name = "deliveryTime") val time: String
)


fun NearestEntity.toRestaurant():
        Restaurant = Restaurant(id=id, name=name, image=logo, time=time)