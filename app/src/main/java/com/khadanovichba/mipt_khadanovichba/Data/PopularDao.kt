package com.khadanovichba.mipt_khadanovichba.Data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface PopularDao{
    @Insert
    suspend fun insert(vararg restaurants: PopularEntity)

    @Query("SELECT * FROM popular")
    suspend fun getRestaurants(): List<PopularEntity>
}

