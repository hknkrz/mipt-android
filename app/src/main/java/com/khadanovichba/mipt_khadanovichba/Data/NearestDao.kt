package com.khadanovichba.mipt_khadanovichba.Data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface NearestDao {
    @Insert
    suspend fun insert(vararg restaurants: NearestEntity)

    @Query("SELECT * FROM nearest")
    suspend fun getRestaurants(): List<NearestEntity>
}
