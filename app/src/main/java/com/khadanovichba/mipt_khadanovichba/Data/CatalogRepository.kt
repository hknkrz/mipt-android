package com.khadanovichba.mipt_khadanovichba.Data

import io.ktor.http.HttpMethod

import io.ktor.client.HttpClient
import io.ktor.client.call.body
import io.ktor.client.request.request
import java.lang.Exception
import javax.inject.Inject
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class CatalogRepository @Inject constructor(
    private val client: HttpClient,
    private val nearestDao: NearestDao,
    private val popularDao: PopularDao,
) {
    suspend fun getCatalog(): Flow<Catalog> {
        return flow{
            try {

                val response = client.request("http://10.0.2.2:5002/catalog") {
                    method = HttpMethod.Get
                }.body<Catalog>()
                nearestDao.insert(*response.nearest.map{
                    it.toNearestEntity()
                }.toTypedArray())
                popularDao.insert(*response.popular.map{
                    it.toPopularEntity()
                }.toTypedArray())
                emit(response)
            } catch (e: Exception) {
                val nearest = nearestDao.getRestaurants()
                val popular = popularDao.getRestaurants()
                emit(Catalog(
                    nearest = nearest.map(NearestEntity::toRestaurant),
                    popular = popular.map(PopularEntity::toRestaurant),
                    commercial = null
                ))
            }
        }

    }

}