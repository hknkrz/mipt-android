package com.khadanovichba.mipt_khadanovichba.Db
import androidx.room.Database
import androidx.room.RoomDatabase
import com.khadanovichba.mipt_khadanovichba.Data.NearestDao
import com.khadanovichba.mipt_khadanovichba.Data.PopularDao
import com.khadanovichba.mipt_khadanovichba.Data.PopularEntity
import com.khadanovichba.mipt_khadanovichba.Data.NearestEntity

@Database(entities = [PopularEntity::class, NearestEntity::class], version = 1)
abstract class RestaurantsDatabase : RoomDatabase() {
    abstract fun popularDao(): PopularDao
    abstract fun nearestDao(): NearestDao
}