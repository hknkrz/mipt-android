package com.khadanovichba.mipt_khadanovichba

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavType
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.compose.NavHost
import androidx.navigation.navArgument
import com.khadanovichba.mipt_khadanovichba.Interface.Screens.ClickRestaurant
import com.khadanovichba.mipt_khadanovichba.Interface.Screens.StartScreen
import com.khadanovichba.mipt_khadanovichba.Interface.Screens.RestaurantScreen
import com.khadanovichba.mipt_khadanovichba.Interface.ViewModels.RestaurantsViewModel
import com.khadanovichba.mipt_khadanovichba.ui.theme.MiptKhadanovichbaTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val navController = rememberNavController()
            MiptKhadanovichbaTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    NavHost(
                        modifier = Modifier,
                        navController = navController,
                        startDestination = "SignUp"
                    ) {
                        composable("SignUp") {
                            StartScreen(navController)
                        }
                        composable("Restaurants"){
                            RestaurantScreen(navController)
                        }
                        composable(
                            "Restaurants/{name}/{deliveryTime}/{imagePath}",
                            arguments = listOf(
                                navArgument("name") { type = NavType.StringType },
                                navArgument("deliveryTime") { type = NavType.StringType },
                                navArgument("imagePath") { type = NavType.StringType }
                            )
                        ) { backStackEntry ->
                            ClickRestaurant(
                                name = backStackEntry.arguments?.getString("name")!!,
                                time = backStackEntry.arguments?.getString("deliveryTime")!!,
                                imagePath = backStackEntry.arguments?.getString("imagePath")!!
                            )
                        }
                    }
                }
            }
        }
    }
}


