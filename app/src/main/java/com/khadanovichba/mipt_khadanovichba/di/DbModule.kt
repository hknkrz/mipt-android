package com.khadanovichba.mipt_khadanovichba.di
import android.content.Context
import androidx.room.Room
import com.khadanovichba.mipt_khadanovichba.Data.NearestDao
import com.khadanovichba.mipt_khadanovichba.Data.PopularDao
import com.khadanovichba.mipt_khadanovichba.Db.RestaurantsDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class DbModule {
    @Provides
    fun provideDatabase(@ApplicationContext context: Context): RestaurantsDatabase
    = Room.databaseBuilder(
        context,
        RestaurantsDatabase::class.java, "RestaurantsDatabase").build()
    @Provides
    fun provideNearestDao(db: RestaurantsDatabase): NearestDao =
        db.nearestDao()
    @Provides
    fun providePopularDao(db: RestaurantsDatabase): PopularDao =
        db.popularDao()

}


