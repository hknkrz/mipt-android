package com.khadanovichba.mipt_khadanovichba.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.serialization.kotlinx.json.*

@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {

    @Provides
    fun client(): HttpClient = HttpClient(CIO) {
        install(ContentNegotiation) {
            json()
        }
    }
}